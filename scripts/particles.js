const sm = window.innerWidth <= 767

let value_area = 2000
let size_value = 12
let quant_value = 120
let distance = 250
let line_width = 1
if(sm){
  value_area = 1500
  size_value = 10
  quant_value = 180
  distance = 190
  line_width = 0.5
}

particlesJS("particles-js", {
  particles: {
    number: {
      value: quant_value,
      density: { enable: true, value_area: value_area },
    },
    color: { value: ["#ff5c58", "#CCCCCC"] },
    shape: {
      type: "circle",
      stroke: { width: 0, color: "#ffffff" },
      polygon: { nb_sides: 3 }
    },
    opacity: {
      value: 2,
      random: true,
      anim: { enable: false, speed: 1, opacity_min: 0.4, sync: false },
    },
    size: {
      value: size_value,
      random: true,
      anim: { enable: true, speed: 10, size_min: 0.4, sync: false },
    },
    line_linked: {
      enable: true,
      distance: distance,
      color: "#CCCCCC",
      opacity: 1,
      width: line_width,
    },
    move: {
      enable: true,
      speed: 1.5,
      direction: "none",
      random: true,
      straight: false,
      out_mode: "bounce", 
      bounce: false,
      attract: {
        enable: false,
        rotateX: 721.5354273894853,
        rotateY: 721.5354273894853,
      },
    },
  },
  interactivity: {
    detect_on: "canvas",
    events: {
      onhover: { enable: false, mode: "repulse" },
      onclick: { enable: true, mode: "push" },
      resize: true,
    },
    modes: {
      grab: { distance: 400, line_linked: { opacity: 1 } },
      bubble: {
        distance: 85.26810729164123,
        size: 365.4347455356053,
        duration: 5.440917322419012,
        opacity: 0.3816762897816322,
        speed: 3,
      },
      repulse: { distance: 97.44926547616141, duration: 0.4 },
      push: { particles_nb: 4 },
      remove: { particles_nb: 2 },
    },
  },
  retina_detect: true,
});
if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array);
