particlesJS("particles-js", {
  particles: {
    number: {
      value: 200,
      density: { enable: true, value_area: 1523.2414578222467 },
    },
    color: { value: "#ff5c58" },
    shape: {
      type: "circle",
      stroke: { width: 0, color: "#ffffff" },
      polygon: { nb_sides: 3 },
      image: { src: "img/github.svg", width: 100, height: 100 },
    },
    opacity: {
      value: 1,
      random: true,
      anim: { enable: false, speed: 1, opacity_min: 0.1, sync: false },
    },
    size: {
      value: 8,
      random: true,
      anim: { enable: false, speed: 40, size_min: 0.1, sync: false },
    },
    line_linked: {
      enable: true,
      distance: 192.40944730386272,
      color: "#6b6868",
      opacity: 0.3367165327817598,
      width: 0,
    },
    move: {
      enable: true,
      speed: 1.5,
      direction: "none",
      random: true,
      straight: false,
      out_mode: "out",
      bounce: false,
      attract: {
        enable: false,
        rotateX: 721.5354273894853,
        rotateY: 721.5354273894853,
      },
    },
  },
  interactivity: {
    detect_on: "canvas",
    events: {
      onhover: { enable: false, mode: "repulse" },
      onclick: { enable: true, mode: "push" },
      resize: true,
    },
    modes: {
      grab: { distance: 400, line_linked: { opacity: 1 } },
      bubble: {
        distance: 85.26810729164123,
        size: 365.4347455356053,
        duration: 5.440917322419012,
        opacity: 0.3816762897816322,
        speed: 3,
      },
      repulse: { distance: 97.44926547616141, duration: 0.4 },
      push: { particles_nb: 4 },
      remove: { particles_nb: 2 },
    },
  },
  retina_detect: true,
});
if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array);
