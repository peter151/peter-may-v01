const menuIcon = document.querySelector('.nav-icon-5')
const menuBox = document.querySelector('#burger-nav')
const mainMenu = document.querySelector('#main-menu')
const wrapper = document.querySelector('#wrapper')
const crossHair = document.querySelector('#crosshair')
let menuOpen = false;

  menuBox.addEventListener('click', (event) => {
    if(!menuOpen){
      menuIcon.classList.toggle("open")
      mainMenu.classList.toggle('open')
      wrapper.classList.toggle('open')
      setTimeout(() => {menuOpen = !menuOpen}, 500)
    }
  });

  document.addEventListener('click', (e) => {
      if(menuOpen){
        menuOpen = !menuOpen
        menuIcon.classList.toggle("open")
        mainMenu.classList.toggle('open')
        wrapper.classList.toggle('open')
      }
  })


  const homeLinkMd = document.querySelector('#md-logo')
  const homeLinkSm = document.querySelector('#logo')
  const offeringLink = document.querySelector('#offerings-btn')

  homeLinkMd.addEventListener('click', () => {
      document.location.href = homeUrl
  })
  homeLinkSm.addEventListener('click', () => {
    document.location.href = homeUrl
})
offeringLink.addEventListener('click', () => {
    document.location.href = offeringsUrl
})
