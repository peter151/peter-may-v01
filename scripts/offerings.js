let initValue
const links = document.querySelectorAll('.offerings-btn')
const content = document.querySelectorAll('.offerings-content')

// use the anchor tag
// console.log(window.location.href)
if(location.hash){
    initValue = location.hash.substring(1)
    // console.log(initValue)
} else {
    initValue = 'leadership-conversations'
}

setUpPage(initValue)


function setUpPage (value) {
    window.scrollTo(0,0)
    // * get the matching button
    let thisLink = [...links].filter(link => link.id === `${value}-btn`)
    // * remove styling from other buttons
    links.forEach(link => {
        link.classList.remove('btn-pm-pink')
        link.classList.add('btn-grey')
    })
    // * add the correct styling the the selected button
    thisLink[0].classList.remove('btn-grey')
    thisLink[0].classList.add('btn-pm-pink')
    
    // * get the matching content
    let thisContent = [...content].filter(content => content.id === `${value}-content`)
    // * hide other content divs
    content.forEach(content =>  content.classList.add ('d-none'))
    // * reveal correct content
    thisContent[0].classList.remove('d-none')
}

window.addEventListener('popstate', function (event) {
	// console.log('the url changed')
    value = location.hash.substring(1)
    setUpPage(value)
});



links.forEach(link => {
    link.addEventListener('click', e => {
        let id = e.target.id
        let value = id.slice(0, -4)
        location.hash = value
        setUpPage(value)
    })
})