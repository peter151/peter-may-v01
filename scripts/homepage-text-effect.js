const textBox = document.querySelector('.typed-text')
    const textToType = ['Consultant', 'Conversations', 'Coach']
    let currentWord = 0;

    function updateWord ()  {
        currentWord ++
        if(currentWord === 3){currentWord = 0}
        typeText(textToType[currentWord])
    }

    function toggleTimer(timer) {
        let lapsedTime = 0
        let toggle = setInterval(() => {
            textBox.classList.toggle('cursor-off')
            if(lapsedTime === timer){
                clearInterval(toggle)
                textBox.classList.remove('cursor-off')
            }
            lapsedTime ++
        }, 200);
    }

    function eraseText (text) {
        let index = -4
        toggleTimer(4)
        let removeType = setInterval(() => {
            if(index){textBox.innerText = text.substring(0, text.length - index)}
            index ++
            if(index === text.length + 1){
                clearInterval(removeType)
                updateWord()
            }
        }, 200)
    }

    function typeText (text) {
        let index = -3
        toggleTimer(3)
        let textType = setInterval(() => {
            textBox.innerText += text.charAt(index)
            index ++
            if(index === text.length){
                clearInterval(typeText)
                eraseText(text)
            }
        }, 200)
    }

    typeText(textToType[currentWord])