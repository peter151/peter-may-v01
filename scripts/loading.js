let enoughTime = false;

document.onreadystatechange = function() {
    if (document.readyState !== "complete") {
        document.querySelector("body").style.visibility = "hidden";
        document.querySelector("#loader").style.visibility = "visible";
    } else if(document.readyState === "complete") {
        document.querySelector("#loader").style.display = "none";
        document.querySelector("body").style.visibility = "visible";
    }
};

/* -------------------------------------------------------------------------- */
/*                                   !testing                                  */
/* -------------------------------------------------------------------------- */

// document.querySelector("body").style.visibility = "hidden";
//         document.querySelector("#loader").style.visibility = "visible";