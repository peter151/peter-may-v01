<?php 
/**
Template Name: Contact page
Template Post Type: page
*/
?>

<?php get_header() ?>



<div class="flex-container">

<div class="container-fluid">
    <div class="body-padding-top"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
                <h1>Contact <span class="text-pm-pink">me</span>.</h1>
        </div>
        <div class="col-12 col-md-4 py-3 mx-auto text-md-center">
            <p>Drop me an email and I will get back to you.</p>
        </div>

        <div class="container"></div>
        <!-- form -->
        <div class="col-12 col-md-6 col-lg-4 mx-auto">
            <?php echo do_shortcode('[contact-form-7 id="74" title="Contact form 1"]') ?>
        </div>
            
    </div>
</div>

<?php get_footer() ?>
</div>