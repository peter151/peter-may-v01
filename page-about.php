<?php 
/**
Template Name: About page
Template Post Type: page
*/
?>

<?php get_header() ?>



<!-- * start of page -->
<div class="body-padding-top"></div>

<div class="container-fluid">
<!--
    /* -------------------------------------------------------------------------- */
    /*                                 banner row                                 */
    /* -------------------------------------------------------------------------- */
 -->
    <div class="container mb-5">
        <div class="row">
            <div class="col-12 text-center">
                <h1>About <span class="text-pm-pink">me</span>.</h1>
            </div>
            <div class="col-12 header-padding-top "></div>
            <div class="col-12 col-md-4 col-lg-2 offset-md-1 offset-lg-2">
                <div id="portrait">
                    <div id="portrait">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PM_mobile_headshot.jpg" alt="Peter May Speaking at a conference in 2019">
                    </div>
                </div>
                <div class="portrait-description"></div>
            </div>
            <div id="about-intro" class="col-12 col-md-6 col-lg-6">
                <p>
                    I am a leadership coach and consultant, with a focus on leadership conversations, cultural interventions, and equitable succession. 
                </p>
                <p>
                    My approach is holistic and people-centred. At the core, it is about understanding a client’s needs and aspirations. 
                </p>
                <p>
                    Together we can solve your and your organization’s most challenging and complex problems.
                </p>
            </div>
        </div>
    </div>

<!--
    /* -------------------------------------------------------------------------- */
    /*                                 experience row                                 */
    /* -------------------------------------------------------------------------- */
 -->

    <div class="container-fluid bg-lightest-grey py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 d-md-none text-center">
                    <h1>My <span class="text-pm-pink">experience</span>.</h1>
                </div>
                <!-- Timeline -->
                <div class="col-11 col-md-5 col-lg-4  order-md-2 timeline-container mt-5">
                    <div class="center-line"></div>
                    <div class="top-circle"></div>
                    <div class="bottom-circle"></div>
                    
                    <!-- Experience element -->
                    <div class="content-experience first-experience">
                        <div><div class="line"></div></div>
                        <span class="company">Deloitte Global</span>
                        <span class="location">, New York & London</span>
                        <p class="position">Chief Human Resources Officer</p>
                    </div>
                    
                    <!-- Experience element -->
                    <div class="content-experience">
                        <div><div class="line"></div></div>
                        <span class="company">Baker McKenzie</span>
                        <span class="location">, New York</span>
                        <p class="position">Global Chief People Officer</p>
                    </div>
                    
                    <!-- Experience element -->
                    <div class="content-experience">
                        <div><div class="line"></div></div>
                        <span class="company">CSIRO</span>
                        <span class="location">, Canberra & Sydney</span>
                        <p class="position">Executive Director, People & Culture</p>
                    </div>
                    
                    <!-- Experience element -->
                    <div class="content-experience">
                        <div><div class="line"></div></div>
                        <span class="company">PwC Consulting</span>
                        <span class="location">, Sydney</span>
                        <p class="position">Asia Pacific Learning Leader</p>
                    </div>
                    
                    <!-- Experience element -->
                    <div class="content-experience">
                        <div><div class="line"></div></div>
                        <span class="company">Institute for Transformational Leadership</span>
                        <span class="location">, Georgetown University</span>
                        <p class="position">Leadership coaching</p>
                    </div>
                </div>
                <!-- Accompanying Text -->
                <div class="col-12 col-md-5 col-lg-4 offset-md-1 offset-lg-2  order-md-1 timeline-text mt-5">
                    <h1 class="d-none d-md-block py-5">My <span class="text-pm-pink">experience</span>.</h1>
                    <p>
                        I bring a vast knowledge and expertise to my work. I have significant international experience and insight.
                    </p>
                    <p>
                        I have worked in the Americas, EMEA, and Asia Pacific. I have held the most senior human resources roles at internationally renowned professional services firms. 
                    </p>
                    <p>
                        I have also been a member of executive leadership teams at Deloitte and Baker McKenzie. 
                    </p>
                    <p>
                        I also led the learning and development team at PwC Consulting in Asia Pacific.  
                    </p>
                </div>
            </div>
        </div>
    </div>



    <!-- 
        /* -------------------------------------------------------------------------- */
        /*                             What I believe row                             */
        /* -------------------------------------------------------------------------- */
     -->

     <div class="container my-5">
        <div class="row">
            <div class="col-12 text-center pb-lg-5">
                <h1>What I <span class="text-pm-pink">believe</span>.</h1>
            </div>

            <div class="col-12 col-md-10 col-lg-8 offset-md-1 offset-lg-3 row ">
                <?php 
                    $data = [
                        'image' => 'people',
                        'heading' => 'Organisations are like people, and like people …',
                        'text' => ''
                    ];
                    get_template_part('template-parts/content', 'believe-icons', $data)
                ?>

                <?php 
                    $data = [
                        'image' => 'complex',
                        'heading' => 'We are more complex and more simple than we believe.',
                        'text' => 'We are the products of our rich personal histories and experiences, but sometimes we get stuck. Stuck in patterns of behaviour. Some of these patterns can be successful and others problematic. '
                    ];
                    get_template_part('template-parts/content', 'believe-icons', $data)
                ?>

                <?php 
                    $data = [
                        'image' => 'resources',
                        'heading' => 'We have more resources than we know.',
                        'text' => 'People are not empty vessels. We have vast inner resources that we aren’t always aware of. When we become aware of them, we can use these to solve the most complex and challenging problems for ourselves and our organizations. '
                    ];
                    get_template_part('template-parts/content', 'believe-icons', $data)
                ?>

                <?php 
                    $data = [
                        'image' => 'common',
                        'heading' => 'We have more in common than we think.',
                        'text' => 'This is especially true when we are working internationally with people from different backgrounds, cultures, and languages. People are always attempting to find a way forward and to take others on the journey with them.   '
                    ];
                    get_template_part('template-parts/content', 'believe-icons', $data)
                ?>

                <?php 
                    $data = [
                        'image' => 'meaning',
                        'heading' => 'We seek meaning more than we acknowledge.',
                        'text' => 'As people, we need to make sense of the world. To find the narratives that work.  We need to have the courage to examine these narratives, to look at things anew, and to refine our understanding of meaning and purpose.'
                    ];
                    get_template_part('template-parts/content', 'believe-icons', $data)
                ?>

                <?php 
                    $data = [
                        'image' => 'attempt',
                        'heading' => 'We attempt to do our best more than others may recognize.',
                        'text' => 'No one turns up to work seeking to do a half-hearted job, rather most of us turn up endeavoring to do our very best.  We need to assume that other people are doing likewise.'
                    ];
                    get_template_part('template-parts/content', 'believe-icons', $data)
                ?>
            </div>
        </div>
     </div>

 <!-- 
        /* -------------------------------------------------------------------------- */
        /*                               My approach button                         */
        /* -------------------------------------------------------------------------- */
     -->
     <div class="container my-5">
         <div class="row">
             <div class="col-12 text-end">
                 <a href="<?php echo get_permalink(16)?>" class="btn shadow-none next-button" >My Approach <next-icon><i class="fa-solid fa-arrow-right-long"></i></next-icon></a>
             </div>
         </div>
     </div>

</div>

<?php get_footer() ?>