<?php 
/**
Template Name: My Offerings page
Template Post Type: page
*/
?>

<?php get_header() ?>

<!-- * start of page -->


<div class="container-fluid bg-lightest-grey text-center pb-2 pb-md-3 mb-0">
    <div class="body-padding-top"></div>
    <h1>My <span class="text-pm-pink">Offerings</span>.</h1>
    <p style="line-height: 0.5em;">Select an option below to review the offering.</p>
</div>



<div id="offering-selection" class="text-center bg-lightest-grey pt-2 px-2 mt-0">

    <div class="d-none d-md-block">
        <button id="leadership-conversations-btn" class="offerings-btn btn mt-2 shadow-none">leadership conversations</button>
        <button id="cultural-interventions-btn" class="offerings-btn btn mt-2 shadow-none">cultural interventions</button>
        <button id="equitable-succession-btn" class="offerings-btn btn mt-2 shadow-none">equitable succession</button>
    </div>

    <div class="dropdown d-md-none pb-3">
        <button class="btn btn-dropdown dropdown-toggle shadow-none" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            My offerings
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="#leadership-conversations">leadership conversations</a></li>
            <li><a class="dropdown-item" href="#cultural-interventions">cultural interventions</a></li>
            <li><a class="dropdown-item" href="#equitable-succession">equitable succession</a></li>
        </ul>
    </div>
</div>




<div class="mt-5">
    <div class="offerings-content" id="leadership-conversations-content" >
            <?php get_template_part('template-parts/content', 'conversations') ?>
    </div>
    <div class="offerings-content d-none" id="cultural-interventions-content" >
        <?php get_template_part('template-parts/content', 'interventions') ?>
    </div>
    <div class="offerings-content d-none" id="equitable-succession-content" >
        <?php get_template_part('template-parts/content', 'succession') ?>
    </div>
</div>

<div class="container my-5">
         <div class="row">
             <div class="col-12 text-end">
                 <a href="<?php echo get_permalink(20)?>" class="btn shadow-none next-button" >Contact <next-icon><i class="fa-solid fa-arrow-right-long"></i></next-icon></a>
             </div>
         </div>
     </div>
<!-- * end of page -->
<script src="<?php echo get_template_directory_uri() ?>/scripts/offerings.js"></script>
<?php get_footer() ?>