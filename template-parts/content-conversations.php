<!-- /* -------------------------------------------------------------------------- */
/*                          Heading and opening para                          */
/* -------------------------------------------------------------------------- */ -->
<div class="container pb-5">

    <!-- ** Cluster -->
        <!-- header row -->
        <div class="col-12 col-md-10 col-lg-6  mx-auto pb-3 row">
            <!-- mobile icon -->
            <div class="col-3 d-md-none">
                <div class="pm-big-icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-conversations/conversations.svg" alt="">
                </div>
            </div>
            <!-- heading -->
            <div class="col-9 col-md-12">
                <h1 class="offering-headline">Leadership Conversations</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-lg-1 offset-md-1 offset-lg-3  d-none d-md-block">
                <div class="pm-big-icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-conversations/conversations.svg" alt="">
                </div>
            </div>
            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 ">
                <p>
                “An executive team member simply needed a person to speak with about what was uppermost in their mind.  Sometimes it was about working with others, sometimes it was about solving a complex organizational problem, and sometimes it was about providing space to share an area of concern.  These conversations enabled the leader to think more clearly and act more decisively.”
                </p>
                <p>
                Leading better begins with a conversation that is open, honest and creative. Sometimes a leadership conversation is all that is needed. It is a safe place for a leader to speak about what is most important to them, to reflect on their own experience, and draw on the experience of another to find a way forward.  Sometimes, more is needed.  This is where leadership coaching, team coaching, and formal learning can accelerate change and shift organizational dynamics for the better.
                </p>
            </div>
        </div>
    <!-- ** End of Cluster -->

</div>

<!-- /* -------------------------------------------------------------------------- */
/*                            Container 2 (grey bg)                           */
/* -------------------------------------------------------------------------- */ -->

<div class="py-5 bg-lightest-grey">
    <div class="container">
        <div class="row">
        
            <!-- ** Cluster -->
        <!-- header row -->
        <div class="col-12 pb-3 row d-flex align-items-center">
            <!-- mobile icon -->
            <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3  ">
                <div class="pm-big-icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-conversations/coaching.svg" alt="">
                </div>
            </div>
            <!-- heading -->
            <div class="col-9 col-md-8 col-lg-5 ">
                <h1>Leadership Coaching </h1>
            </div>
        </div>

        <!-- body copy -->
        <div class="col-12 col-md-8 col-lg-5  offset-md-3 offset-lg-4 ">
            <p>
            “A senior medical doctor took on a new leadership role in a large hospital. We worked together to navigate the organizational complexities of the hospital and to successfully lead a team of highly qualified and experience professionals.”
            </p>
            <p>
            We start with mutually agreed upon objectives between the organization’s sponsor and the individual. Sometimes the objectives are agreed with the individual only. The objectives include:
            </p>
            <ul>
                <li>a successful transition to a new role</li>
                <li>improving individual and team performance</li>
                <li>addressing issues that are hard to fix </li>
            </ul>
            <p>
            Formal assessments, stakeholder interviews, and 360-degree feedback create a more nuanced understanding of a person’s strengths and the challenges that need to be addressed.  Coaching is typically time-bound, however, both the timing and objectives can be adapted to meet the needs of the organization and the person. 
            </p>
        </div>
    <!-- ** End of Cluster -->


        </div>
    </div>
</div>


<!-- /* -------------------------------------------------------------------------- */
/*                                 Container 3                                */
/* -------------------------------------------------------------------------- */     -->
<div class="container py-5">

     <!-- ** Cluster -->
        <!-- header row -->
        <div class="col-12 pb-3 row d-flex align-items-center">
            <!-- mobile icon -->
            <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 ">
                <div class="pm-big-icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-conversations/teams.svg" alt="">
                </div>
            </div>
            <!-- heading -->
            <div class="col-9 col-md-8 col-lg-5 ">
                <h1>Leading Teams </h1>
            </div>
        </div>

        <!-- body copy -->
        <div class="col-12 col-md-8 col-lg-5  offset-md-3 offset-lg-4 ">
            <p>
            “A COO wanted to improve the team dynamics of the organization’s finance department. Working together, I undertook an assessment of the team, used a highly regarded model for team understanding and development, and ran a workshop to align the team around a common vision and mission. The team began a new journey with greater collaboration and productivity.”
            </p>
            <p>
            Team coaching also begins with mutually agreed objectives with the organization’s sponsor or team leader. Team assessments and team models are helpful for increasing the understanding of interpersonal dynamics and for improving effectiveness. Teams do better if there’s a unifying purpose and if team roles are clear. Coaching, especially if done over time, helps teams move to a better place where individuals become more interdependent and where they are anchored by a shared vision and a common purpose. 
            </p>
        </div>
    <!-- ** End of Cluster -->
</div>

<!-- /* -------------------------------------------------------------------------- */
/*                            Container 4 (grey bg)                           */
/* -------------------------------------------------------------------------- */ -->

<div class="col-12 py-5 bg-lightest-grey">
    <div class="container">


    <!-- ** Cluster -->
        <!-- header row -->
        <div class="col-12 pb-3 row d-flex align-items-center">
            <!-- mobile icon -->
            <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 ">
                <div class="pm-big-icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-conversations/growing.svg" alt="">
                </div>
            </div>
            <!-- heading -->
            <div class="col-9 col-md-8 col-lg-5 ">
                <h1>Learning and Growth </h1>
            </div>
        </div>

        <!-- body copy -->
        <div class="col-12 col-md-8 col-lg-5  offset-md-3 offset-lg-4 ">
            <p>
                Our work is about building individual, team and organizational leadership acuity and strength. 
            </p>
            <p>
            <b>People Leadership:</b> This is formal leadership development, with a difference. Programs that bring real change are delivered over time and are complimented by coaching. Each program has three to four smaller sessions delivered over a nine-month period, supported by one-on-one coaching.  Leadership topics to explore include enterprise leadership, leading teams, creating psychological safety, cross-border collaboration, leading and influencing in a matrix. 
            </p>
            <!-- image with link -->
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-conversations/diam-1.png" alt="" class="diagram img-fluid">
                <div class="d-flex justify-content-end">
                    <a href="<?php echo get_template_directory_uri() ?>/assets/pdfs/Expanded_people_leadership.pdf" target="_blank" class="d-block ml-5 mt-3">View full diagram <i class="fa-solid fa-square-arrow-up-right"></i></a>
                </div>
                <div class="py-3"></div>
            <!-- end of image with link -->
            <p>
            <b>Client Leadership:</b> This focuses on developing profound relationships with clients. It increases profitable work and strengthens the client account. It involves agreeing targets for growth, a plan to get there, and the necessary skills needed to deliver. The skills of client empathy, listening and pitching are key for success, and are integral to the program. 
            </p>
            <!-- image with link -->
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-conversations/diam-2.png" alt="" class="diagram img-fluid">
                <div class="d-flex justify-content-end">
                    <a href="<?php echo get_template_directory_uri() ?>/assets/pdfs/Expanded_client_leadership.pdf" target="_blank" class="d-block ml-5 mt-3">View full diagram <i class="fa-solid fa-square-arrow-up-right"></i></a>
                </div>
                <div class="py-3"></div>
            <!-- end of image with link -->
        </div>
    <!-- ** End of Cluster -->

    </div>
</div>