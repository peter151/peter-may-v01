<div class="container pb-5">
    <div class="col-12 col-md-10 col-lg-6 col-cd-4 mx-auto pb-3 row">
        <div class="col-12 text-center pb-3">
            <h1 class="offering-headline">Equitable Succession</h1>
        </div>
        <div class="col-12">
            <p>
                “The Global CEO of a large professional services firm thought that it was critical to nurture the next generation of diverse talent and to ensure continuity at the most senior levels in the firm. My work was to design and deliver a program that enabled the very best people to be identified and developed, to be ready to step into the most senior roles.” 
            </p>
            <h2 class="smaller text-center pt-5">Succession Management </h2>
            <p>
                Succession management is about nurturing the next generation of diverse talent and enabling them to succeed. My work here involves: 
            </p>
        </div>
        <div class="col-12 mx-auto pt-5 text-center">
            <!-- image with link -->
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/succession/succession.png" alt="" class="diagram-big img-fluid">
            <div class="d-flex justify-content-end">
                <a href="<?php echo get_template_directory_uri() ?>/assets/pdfs/Expanded_succession.pdf" target="_blank" class="d-block ml-5 mt-3">View full diagram <i class="fa-solid fa-square-arrow-up-right"></i></a>
            </div>
            <div class="py-3"></div>
            <!-- end of image with link -->
        </div>
    </div>
</div>


<!-- /* -------------------------------------------------------------------------- */
/*                            Container 2 (grey bg)                           */
/* -------------------------------------------------------------------------- */ -->

<div class="py-5 bg-lightest-grey">
    <div class="container">
        <div class="row">
        
            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 pt-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <h1>1.</h1>
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3 d-flex align-items-center">
                    <p><b>Determining future capabilities:</b></p>
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                    This is the blueprint on which the succession plan rests. The strategy entails scanning the horizon to determine organizational capabilities and individual competencies. This engages the Board, the Executive, the CHRO, Chief Inclusion & Diversity Officer, and senior leaders. A talent map is produced that sets the course for future success.
                </p>
            </div>
            <!-- ** End of Cluster -->

            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 pt-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <h1>2.</h1>
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3 d-flex align-items-center">
                    <p><b>Identifying “Next-Gen” talent:</b></p>
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                Here we identify potential successors for key roles. This process is broad and takes into account a multiplicity of factors: incumbency, diversity, human resources, executive-team members and, of course, people who have expressed a desire to advance. This net is cast wide so as not to disadvantage minorities or to overlook less obvious choices.  
                </p>
            </div>
            <!-- ** End of Cluster -->

            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 pt-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <h1>3.</h1>
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3 d-flex align-items-center">
                    <p><b>Assess identified talent and develop a plan:</b></p>
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                Once a pool of people have been identified, the next stage is to assess those identified persons against the talent profile (as per step 1). This assessment is focused on growth, learning, and career enhancement. It will involve 360-degree feedback, formal assessment, conversations with incumbents, and a development plan. At this point a “ready now/ready later” assessment can be made. Above all, this is a human process. 
                </p>
            </div>
            <!-- ** End of Cluster -->


            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 pt-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <h1>4.</h1>
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3 d-flex align-items-center">
                    <p><b>Executing the plan:</b></p>
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                If equitable succession planning is to become a reality, it needs to move to an executed development plan for the individual, otherwise nothing changes. The plan can comprise many elements:  formal learning and development 	&#8212 a course, a program, leadership coaching, exposure to different parts of the organization. New experiences that aim to broaden and deepen organizational understanding. This is not “one size fits all.” This is a tailored plan to meet the needs of the individual.   
                </p>
            </div>
            <!-- ** End of Cluster -->

            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 pt-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <h1>5.</h1>
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3 d-flex align-items-center">
                    <p><b>Review and evaluate:  </b></p>
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                This step is on everyone’s slide, but is rarely done. We must step back and ask: Have we achieved our goals? Is succession planning really working? Is it more equitable?  Just as we seek to assess individuals against a talent profile, we assess the organizational capabilities against its strategic objectives. Is the profile of leadership changing and changing for the better? Is it reflecting the rich diversity of both organization and society?     
                </p>
            </div>
            <!-- ** End of Cluster -->


        </div>
    </div>
</div>