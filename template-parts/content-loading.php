<!-- Template part for the loading page -->

<div id="loader">
    <div class="container-loader">
        <div class="row">
            <div class="col-12 text-center mt-5">
                <h1 class="text-pm-pink">Peter May</h1>
            </div>
            <div class="col-12 text-center">
                <h4 class="text-medium-grey">Loading...</h4>
            </div>
            <div class="loader-container">
                <div class="wrapper">
                    <div class="loading">
                        <div class="inner_circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>