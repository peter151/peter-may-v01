<div class="container pb-5">
    <div class="col-12 col-md-10 col-lg-6 col-cd-4 mx-auto pb-3 row">
        <div class="col-12 text-center pb-3">
            <h1 class="offering-headline">Cultural Interventions</h1>
        </div>
        <div class="col-12">
        <p>
                “An executive team was working well, but many on the team felt it could work much better. They felt stuck. The team recognized that there were opportunities to improve collaboration, to have more candid conversations and to increase the level of trust. I ran a series of workshops to address these opportunities. The end result was a set of unifying principles that enabled the team to work better together and perform at even higher levels.”
            </p>
            <p>
                Culture matters, but it is intangible. It can make or break organizations. It is held in the hearts and minds of people. It shows up in everyday interactions, in conversations, in budgets. It is pervasive and needs managing. There are a number of interventions I run to transform culture and drive change:  
            </p>
        </div>
        <div class="col-12 mx-auto pt-5 text-center">
            <!-- image with link -->
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-interventions/diam-1.png" alt="" class="diagram-big img-fluid">
            <div class="d-flex justify-content-end">
                <a href="<?php echo get_template_directory_uri() ?>/assets/pdfs/Expanded_facilitation.pdf" target="_blank" class="d-block ml-5 mt-3">View full diagram <i class="fa-solid fa-square-arrow-up-right"></i></a>
            </div>
            <div class="py-3"></div>
            <!-- end of image with link -->
        </div>
    </div>
</div>


<!-- /* -------------------------------------------------------------------------- */
/*                            Container 2 (grey bg)                           */
/* -------------------------------------------------------------------------- */ -->

<div class="py-5 bg-lightest-grey">
    <div class="container">
        <div class="row">
        
            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 py-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-interventions/direction.svg" alt="">
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3">
                    <h2 class="smaller">Facilitating Direction and Alignment</h2 class="smaller">
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                    It starts with a recognition that “things are not working” or “things are okay, but we can do better,” or “we are not really on one page when it comes to strategy, can you help us get there?” The work here involves bringing people together—the Board, Executive Team, Leadership Team—and, through a process of facilitation, people determine “what matters most” and agree a way forward. 
                </p>
            </div>
            <!-- ** End of Cluster -->

            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 py-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-interventions/connection.svg" alt="">
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3">
                    <h2 class="smaller">Facilitating Connection and Growth</h2 class="smaller">
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                    This work is about enabling people to talk again. It is needed when things are broken, or where trust has been eroded, or where past hurts need to be healed. Wounds are healed and people find a way forward. My work involves a series of workshops, meetings and focus groups where listening to one another in a structured and respectful way allows us all to learn and find ways to move forward. 
                </p>
            </div>
            <!-- ** End of Cluster -->

            <!-- ** Cluster -->
            <!-- header row -->
            <div class="col-12 py-3 row d-flex align-items-center">
                <!-- mobile icon -->
                <div class="col-3 col-md-2 col-lg-1 offset-md-1 offset-lg-3 offset-cd-4">
                    <div class="pm-big-icon">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/leadership-interventions/strategic.svg" alt="">
                    </div>
                </div>
                <!-- heading -->
                <div class="col-9 col-md-8 col-lg-5 col-cd-3">
                    <h2 class="smaller">Facilitating Strategic Change</h2 class="smaller">
                </div>
            </div>

            <!-- body copy -->
            <div class="col-12 col-md-8 col-lg-5 col-cd-3 offset-md-3 offset-lg-4 offset-cd-5">
                <p>
                    Everyone wants to change, but making change is difficult. It requires all the usual ingredients—clear direction, stakeholder readiness, alignment of people, systems, processes—but it also requires courage. This is not a rigid view of courage but one that is adaptive to reality. Courage is necessary if people are serious about change. The strategic work I do focuses on:
                </p>
                <ul>
                    <li>organizational culture</li>
                    <li>learning and development</li>
                    <li>diversity, inclusion, equity, and anti-racism</li>
                </ul>
                <p>
                    While there are many more elements that make an integrated people strategy, these elements are fundamental if change is to become a reality.
                </p>
            </div>
            <!-- ** End of Cluster -->


        </div>
    </div>
</div>