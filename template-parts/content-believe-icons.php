 <!-- believe text and icon -->
 <div class="col-12 row py-4 px-md-3">
    <div class="col-3 col-md-2 text-center">
        <div class="pm-icon">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/believe/<?php echo $args['image'] ?>.svg" alt="">
        </div>
    </div>
    <div class="col-9 pt-1 pm-icon-text">
        <h4><?php echo $args['heading'] ?></h4>
        <p class="pt-2">
            <?php echo $args['text'] ?>
        </p>
    </div>
</div>
<!-- icon close -->