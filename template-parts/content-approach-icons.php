<div class="<?php echo $args['background'] ?> py-5">
        <div class="container">
            <div class="row">
                <div class="col-3 col-lg-2 offset-lg-2 d-flex justify-content-center">
                    <div class="pm-icon">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/approach/<?php echo $args['image'] ?>.svg" alt="">
                    </div>
                </div>
                <div class="col-9 col-lg-6">
                        <h4><?php echo $args['heading'] ?></h4>
                        <ul class="mt-4">
                            <?php foreach ($args['text'] as $item){
                                echo "<li>$item</li>";
                            } ?>
                        </ul>
                </div>
            </div>
        </div>
    </div>