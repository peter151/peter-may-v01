<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PG_May
 */

?>

<button onclick="topFunction()" id="to-top" class="btn btn-social shadow-none">
	<i class="fa-solid fa-arrow-up"></i>
</button>

<div class="footer d-flex flex-column justify-content-center">
	<div id="footer-content" class="mx-auto">
		<div class="title text-center mt-3">
			<a href="<?php get_permalink(get_page_by_title('Peter May - Home')) ?>"><h3>Peter May</h3></a>
		</div>
		
		<div class="menu-items mx-auto">
			<nav id="site-navigation" class="main-navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav>
		</div>
		<div class="button-group mx-auto">
			<a href="<?php echo get_permalink(18) ?>#leadership-conversations" class="mt-2 btn btn-pm-pink">leadership conversations</a>
			<a href="<?php echo get_permalink(18) ?>#cultural-interventions" class="mt-2 btn btn-pm-pink">cultural interventions</a>
			<a href="<?php echo get_permalink(18) ?>#equitable-succession" class="mt-2 btn btn-pm-pink">equitable succession</a>
		</div>
		<div class="socials-group mx-auto">
			<a href="https://www.linkedin.com/in/petermay/" target="_blank" class="btn btn-social"><i class="fa-brands fa-linkedin"></i></a>
			<a href="mailto:peter@pgmay.com" target="_blank" class="btn btn-social"><i class="fa-solid fa-envelope"></i></a>
		</div>
	</div>
	<div id="footer-banner">
		<div id="las-footer" class="d-flex align-items-center justify-content-start justify-content-md-center">
			<div id="my-logo">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/LAS_logo.svg" alt="">
			</div>
			<div class="text-start">
				<a class="mx-0 text-start" href="https://www.lauraannseal.com" target="_blank">Created by Laura Ann Seal</a>
			</div>
		</div>
	</div>
</div>

		</div><!-- end of #wrapper -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri() ?>/scripts/loading.js"></script>


<script>
	const homeUrl = '<?php echo get_permalink(9) ?>'
	const offeringsUrl = '<?php echo get_permalink(18) ?>'


	const toTopBtn = document.querySelector('#to-top')

	window.onscroll = function() {scrollFunction()}

	function scrollFunction() {
		if(document.body.scrollTop > 100  || document.documentElement.scrollTop > 100){
			toTopBtn.style.bottom = '50px'
		} else {
			toTopBtn.style.bottom = '-200px'
		}
	}

	function topFunction() {
		document.body.scrollTop = 0
		document.documentElement.scrollTop = 0
		toTopBtn.classList.remove('hover')
	}
</script>

<!-- bootstrap jquery -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

<!-- menu icon change -->
<script src="<?php echo get_template_directory_uri() ?>/scripts/menu.js"></script>
</body>
</html>
