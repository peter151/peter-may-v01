<?php 
/**
Template Name: My Approach page
Template Post Type: page
*/
?>

<?php get_header() ?>

<!-- * start of page -->
<div class="body-padding-top"></div>

<div class="container-fluid">
    <!-- heading -->
    <div class="container mb-5">
        <div class="row">
            <div class="col-12 text-center">
                <h1>My <span class="text-pm-pink">approach</span>.</h1>
            </div>
            <div class="col-12 col-md-10 col-lg-5 mx-auto mt-3 text-md-center">
                <p>
                My approach is based on in-depth listening and understanding, leading to action and solution. 
                </p>
            </div>
        </div>
    </div>

    <!-- 
        /* -------------------------------------------------------------------------- */
        /*                           icons and descriptions                           */
        /* -------------------------------------------------------------------------- */
     -->
    <?php 
        $data = [
            'background' => 'bg-lightest-grey',
            'image' => 'listening',
            'heading' => 'Listening & Framing',
            'text' => array(
                "Understanding what’s important; where people are stuck; what needs to change",
                "Framing the approach",
                "Aligning with stakeholders"
            )
        ];
        get_template_part('template-parts/content', 'approach-icons', $data)
    ?>

    <?php 
        $data = [
            'background' => '',
            'image' => 'contracting',
            'heading' => 'Agreeing & Contracting',
            'text' => array(
                "Agreeing the focus and scope of the work; how we will work; how we will engage others",
                "Contracting on outcomes and milestones with stakeholders"
            )
        ];
        get_template_part('template-parts/content', 'approach-icons', $data)
    ?>

    <?php 
        $data = [
            'background' => 'bg-lightest-grey',
            'image' => 'acting',
            'heading' => 'Acting & Adapting',
            'text' => array(
                "Moving into action",
                "Adapting the approach as we move deeper into the engagement",
                "Aligning with stakeholders along the way"
            )
        ];
        get_template_part('template-parts/content', 'approach-icons', $data)
    ?>

    <?php 
        $data = [
            'background' => '',
            'image' => 'reviewing',
            'heading' => 'Reviewing & Evaluating',
            'text' => array(
                "Determining “how did we do?” What enduring lessons have been learned?",
                "Evaluating the outcomes with stakeholders"
            )
        ];
        get_template_part('template-parts/content', 'approach-icons', $data)
    ?>

     <!-- 
        /* -------------------------------------------------------------------------- */
        /*                               Offerings button                         */
        /* -------------------------------------------------------------------------- */
     -->
     <div class="container my-5">
         <div class="row">
             <div class="col-12 text-end">
                 <a href="<?php echo get_permalink(18)?>" class="btn shadow-none next-button" >Offerings <next-icon><i class="fa-solid fa-arrow-right-long"></i></next-icon></a>
             </div>
         </div>
     </div>
</div>
<?php get_footer() ?>