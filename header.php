<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PG_May
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- google analytics -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TZ3RTND');</script>
	<!-- End Google Tag Manager -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- open graph -->
	<meta property="og:title" content="Peter May - Leadership Coach" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="<?php echo get_template_directory_uri() ?>/assets/images/og_image.png" />
	<meta property="og:url" content="https://www.pgmay.com" />

	<!-- meta data for SEO -->
	<meta name="description" content="I am a leadership coach and consultant. Together we can solve your and your company’s most challenging and complex problems.">

	<!-- bootstrap -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

	<!-- font awesome -->
	<script src="https://kit.fontawesome.com/112fa48a56.js" crossorigin="anonymous"></script>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>



<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'pg-may' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="menu-icons">
			<div id="logo" class="inline-flex-center-center d-md-none">PM</div>
			<div id="md-logo" class="inline-flex-center-center d-none d-md-inline-flex">Peter May</div>
			<div class="flexBox d-md-flex flex-grow-1 d-none"></div>
			<div id="offerings-btn" class="inline-flex-center-center d-none d-md-inline-flex">
				offerings
			</div>
			<div id="burger-nav" class="inline-flex-center-center checked">
				<div class="icon nav-icon-5">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="main-menu" class="d-flex flex-column">
		<div class="menu-items mx-auto mt-auto">
			<nav id="site-navigation" class="main-navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav>
		</div>
		<div class="button-group mx-auto mb-auto">
			<a href="<?php echo get_permalink(18) ?>#leadership-conversations" class="mt-2 btn btn-pm-pink">leadership conversations</a>
			<a href="<?php echo get_permalink(18) ?>#cultural-interventions" class="mt-2 btn btn-pm-pink">cultural interventions</a>
			<a href="<?php echo get_permalink(18) ?>#equitable-succession" class="mt-2 btn btn-pm-pink">equitable-succession</a>
		</div>
		<div class="socials-group mx-auto mb-5">
			<a href="https://www.linkedin.com/in/petermay/" target="_blank" class="btn btn-social"><i class="fa-brands fa-linkedin"></i></a>
			<a href="mailto:peter@pgmay.com" target="_blank" class="btn btn-social"><i class="fa-solid fa-envelope"></i></a>
		</div>
	</div>

	<div id="wrapper">


	<!--  * loading container -->
	<?php get_template_part('template-parts/content', 'loading') ?>
