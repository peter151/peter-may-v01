<?php 
/**
Template Name: Home page
Template Post Type: page
*/
?>

<?php get_header() ?>

<!-- 
    /* -------------------------------------------------------------------------- */
    /*                               Network banner                               */
    /* -------------------------------------------------------------------------- */
 -->
<div id="network-container" class="container-fluid">
        <div id="particles-js"></div>
        <div id="opaque-layer"></div>
        <div id="particles-text">
            <h1><span class="pm-pink">Change</span> is tough,<br class="d-lg-none"> but <span class="pm-pink">possible</span>.</h1>
            <div id="typewriter" class="rounded-top-only mx-auto">
                <h1 id="website-title">Peter May</h1>
                <div class="button-group mx-auto">
                    <a href="<?php echo get_permalink(18) ?>#leadership-conversations" class="mt-2 btn btn-pm-pink">leadership conversations</a>
                    <a href="<?php echo get_permalink(18) ?>#cultural-interventions" class="mt-2 btn btn-pm-pink">cultural interventions</a>
                    <a href="<?php echo get_permalink(18) ?>#equitable-succession" class="mt-2 btn btn-pm-pink">equitable succession</a>
                </div>
            </div>
        </div>
</div>

<!-- 
    /* -------------------------------------------------------------------------- */
    /*                               Stuck container                              */
    /* -------------------------------------------------------------------------- */
 -->
 <div class="homepage-divider"></div>
 
<div class="container mt-5">
    <div class="row">
        <div class="col-12 col-md-4 offset-md-1">
            <div id="stuck-animation">
                <picture>
                    <!-- <source srcset="<?php echo get_template_directory_uri() ?>/assets/anim/maze.webp" type="image/webp"> -->
                    <source srcset="<?php echo get_template_directory_uri() ?>/assets/anim/maze.png" type="image/png">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/anim/maze.png" alt="Maze">
                </picture>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-5 px-3 px-md-0">
            <h2 class="mt-5 mb-3 my-md-3 mt-lg-5">Stuck?</h2>
            <p>
            From time to time, all of us have experienced being stuck. The moment when our aspirations come up against a tough reality. I help individuals, teams and organizations to get “unstuck,” to get moving and find a way forward. We move when we achieve our milestones, lead through collaboration, and embody a culture of inclusion, trust and excellence.
            </p>
        </div>
    </div>
</div>

<div class="homepage-divider"></div>
<!-- 
    /* -------------------------------------------------------------------------- */
    /*                          Leadership coach pop out                          */
    /* -------------------------------------------------------------------------- */
 -->

 <div class="container mt-5" id="pop-over-container">
     <div class="row px-2">
         <div class="col-12 col-md-8 bg-lightest-grey" id="pop-out">
             <div class="row">
                 <div class="col-12 col-md-8 offset-md-3">
                        <h4>I am a leadership coach and consultant</h4>
                        <p class="my-4 mt-md-1 mb-md-3">Together we can solve your and your organization's most challenging and complex problems.</p>
                        <div class="text-center text-md-start">
                            <a href="<?php echo get_permalink(get_page_by_title('About Me')) ?>" class=" mt-2 btn btn-pm-pink">Learn about me</a>
                        </div>
                 </div>
             </div>
        </div>
     </div>
 </div>

 <!-- 
     /* -------------------------------------------------------------------------- */
     /*                             Paper plane section                            */
     /* -------------------------------------------------------------------------- */
  -->
  <div class="homepage-divider"></div>

  <div class="container mt-5" id="paper-plane">
      <div class="row d-lg-flex align-items-center justify-content-start">
          <div class="col-12 py-5 d-md-none">
              <h2 class="smaller">We have more resources than we know.</h2>
          </div>
          <div class="col-12 col-md-4 col-lg-3 offset-md-1 offset-lg-2">
            <picture>
                <!-- <source srcset="<?php echo get_template_directory_uri() ?>/assets/anim/plane.webp" type="image/webp"> -->
                <source srcset="<?php echo get_template_directory_uri() ?>/assets/anim/plane.gif" type="image/png">
                <img src="<?php echo get_template_directory_uri() ?>/assets/anim/plane.gif" alt="" class="w-100">
            </picture>
          </div>
          <div class="col-12 col-md-6 col-lg-5 pt-5">
            <h2 class="smaller d-none d-md-block mb-3 mb-lg-5">We have more resources than we know.</h2>
              <p>
                  <b>
                    We are the products of our rich personal histories and experiences. 
                  </b>
              </p>
              <p>
                My approach is based on in-depth listening and understanding, leading to action and solution. 
              </p>
              <div class="text-center text-md-start my-lg-5">
                <a href="<?php echo get_permalink(get_page_by_title('My Approach')) ?>" class=" mt-2 btn btn-pm-pink">My approach</a>
             </div>
          </div>
      </div>
  </div>


<!-- 
    /* -------------------------------------------------------------------------- */
    /*                              Offerings section                             */
    /* -------------------------------------------------------------------------- */
 -->
 <div class="homepage-divider"></div>
 <div class="py-5"></div>

 <div class="container-fluid bg-between-lighter-lightest-grey pb-5 mb-5">
     <div class="container">
        <div class="row pb-2">
                <div class="col-6 col-md-4 rounded-bottom-only mx-auto mb-5 text-center">
                    <h1 class="faded uppercase">Offerings</h1>
                </div>
        </div>
        <div class="homepage-divider"></div>
        <div class="container">
            <div class="row col-md-10 col-lg-12 mx-auto">
                <div class="col-11 col-md-6 col-lg-4 mx-auto mb-4 mx-md-0">
                    <div class="offering d-flex flex-column">
                        <h2>Leadership conversations</h2>
                        <p>Leading better begins with a conversation that is open, honest and creative. Sometimes a leadership conversation is all that is needed.</p>
                        <div class="flex-grow-1"></div>
                        <div class="text-end mt-auto">
                            <a href="<?php echo get_permalink(18)?>#leadership-conversations" class="btn shadow-none mb-3">
                                <next-home-icon><i class="fa-solid fa-arrow-right-long"></i></next-home-icon>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-11 col-md-6 col-lg-4 mx-auto mb-4 mx-md-0">
                    <div class="offering d-flex flex-column">
                        <h2>Cultural interventions</h2>
                        <p>Culture matters, but it is intangible. It can make or break organizations. It is held in the hearts and minds of people. It shows up in everyday interactions, in conversations, in budgets.</p>
                        <div class="flex-grow-1"></div>
                        <div class="text-end mt-auto">
                            <a href="<?php echo get_permalink(18)?>#cultural-interventions" class="btn shadow-none mb-3">
                                <next-home-icon><i class="fa-solid fa-arrow-right-long"></i></next-home-icon>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-11 col-md-6 col-lg-4 mx-auto mb-4 mx-md-0">
                    <div class="offering d-flex flex-column">
                        <h2>Equitable succession</h2>
                        <p>Succession management is about nurturing the next generation of diverse talent and enabling them to succeed.</p>
                        <div class="flex-grow-1"></div>
                        <div class="text-end mt-auto">
                            <a href="<?php echo get_permalink(18)?>#equitable-succession" class="btn shadow-none mb-3">
                                <next-home-icon><i class="fa-solid fa-arrow-right-long"></i></next-home-icon>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>
     <div class="homepage-divider"></div>
 </div>

 <div class="d-md-none d-lg-block homepage-divider"></div>

<script src="<?php echo get_template_directory_uri() ?>/scripts/particles.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/scripts/particles.js"></script>
<!-- <script src="<?php echo get_template_directory_uri() ?>/scripts/homepage-text-effect.js"></script> -->
<?php get_footer() ?>